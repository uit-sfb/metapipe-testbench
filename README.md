# Metapipe-testbench

Testbench for Metapipe

## Usage

```
rm -rf reports
./start.sh [nextflow_options]
```

Note: the datasets could not be stored in git LFS because they exceede GitLab limit of 9.8 GB.

### Common options

- `-resume`
- `-latest` or `-r <tag>`


## Monitoring

```
tail -f ./reports/trace.txt
```
```
watch docker ps
```

## Formating trace in Excel

- Linux timestamp -> date: `=(((G2/60000)/60)/24)+DATE(1970,1,1)` and then format cells to appropriate date
- ms -> duration: `=CONCATENATE(TEXT(INT(H2/1000)/86400,"hh:mm:ss"),".",H2-(INT(H2/1000)*1000))`and then format to appropriate date
- byte -GB: `=IF(L2>1024*1024*1024,TRUNC(L2/1024/1024/1024,2)&" GB", IF(L2>1024*1024, ROUND(L2/1024/1024,0)&" MB", ROUND(L2/1024,0)&" KB"))`
