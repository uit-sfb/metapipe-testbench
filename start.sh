#!/bin/bash

THIS_PATH="$( cd "$( dirname "$0" )" && pwd )"

if [[ -d "$THIS_PATH/reports" ]]; then
  echo "Please delete the directory reports before continuing."
  exit 1
fi
mkdir -p "$THIS_PATH/reports"
mkdir -p "$THIS_PATH/target" && cd "$THIS_PATH/target"
rm -f nohup.out
nohup nextflow run http://gitlab.com/uit-sfb/metapipe -c "$THIS_PATH/nextflow.config" \
  "$@" \
  -with-trace "$THIS_PATH/reports/trace.tsv" -with-timeline "$THIS_PATH/reports/timeline.html" -with-report "$THIS_PATH/reports/report.html" \
  -params-file "$THIS_PATH/params.yml" \
  --refdbDir /home/.metapipe/refdb &
echo $! > pid.txt